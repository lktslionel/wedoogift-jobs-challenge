# Wedoogift Search Insights

## Contents <!-- omit in toc -->

- [1 Requirements](#1-requirements)
  - [1.1 Functional](#11-functional)
  - [1.1 Non-Functional](#11-non-functional)
- [2 Research](#2-research)
- [3 Proposal](#3-proposal)
  - [3.1 Archtiecture Diagram](#31-archtiecture-diagram)
  - [3.2 Components](#32-components)
  - [3.3 Workflow](#33-workflow)
    - [Steps](#steps)
  - [3.4 Additional Notes](#34-additional-notes)
- [4 References](#4-references)


<br>

## 1 Requirements

### 1.1 Functional

To sumarize [the problem](problem.md), we want to be able get insights about brands popularity regarding user's searches. By insights, we mean knowing:

1. The top 10 most popular brands
2. The number of users by brands names
3. The Top 3 most popular brands for a given user

The solution must provide a user friendly interface to visualize those insights.


Not to forget the necessity to satifiy the following specifications:

* Users are able to look for as many brands he want into the search bar.
* The search bar doesnt' provide any autocomplete feature
* We have `1.7M` users but `60K` active ones
* Emphasis should be made on "User satisfaction"
* Around `30` users will consume those insights
* Users need to be authenticated to access those insights
* The RPO(Recovery Point Objective) is `1h`
* The data retention period is `4 months`

### 1.1 Non-Functional

Regarding the specifications mentioned above and the focus on user satisfaction, we choose to take as non-functional requirements the ones below:

* **Usability**: Ease and simple to use
* **Availability**: highly Available
* **Security**: Access to insights must be secured

<br>

## 2 Research

To come up with a solution this problem, I first, strated buy brainstroming any ossbile solution available on AWS.

So, I came up with this mind-map drawing:

![Wedoogift Search Solution Research](./assets/wedoogift-search-solution-research.png)

As you've probably noticed, we have planty of solution to this problem. For the exercise we chosse to go with the simple one, without compromising on the requirements (FRs/NFRs).

Therefore we choose the following solution:

![Wedoogift Search Solution Research Choice](./assets/wedoogift-search-solution-research-selected.png)




## 3 Proposal

### 3.1 Archtiecture Diagram 

The diagram below describe the complete solution. 

![Wedoogift Search Arch Diag](./assets/solution.drawio.png)

### 3.2 Components

It is compose of the folowing components:

* **Route53**: To provide user friendly domain names for wedoogift (existing) and Kibana (dasshboard to see insights)
* **Wedoogift ECS Cluster**: The existing application that provides the search bar for user to look for brands by name.
* **CloudWatch Logs**: To stores Wedoogift application logs including search event logs. We will use cloudwatch logs subscription filter to forward those search events to the elasticsearch cluster[1].
* **OpenSearch Cluster**: To store search events related to user activities on the search bar
* **Kibana (OpenSearch)**: Dashboard provided to weddogift internal users to show search brand insights
* **AWS Cognito**: Provides a user pool and act as an identity provider for Kibana. it will make sure only authenticated users have access to the insights dashboard [2].

### 3.3 Workflow

To make this solution possible. The application should add a log entry related to search events. An example of this log entry can be:

```json
{
    "event_id": "<event-id>",
    "event_type": "search",
    "timestamp": "<timestamp>",
    "event_details": {
        "user_id": "<user-id>",
        "brands_names": ["<brand-name>", ...]
    }
    ...
}
```

#### Steps

1. When a user is looking for some brands via the application, the code logic will log the event and that event will be forwarded to Cloudwatch logs. 
2. Search event logs are then forwarded to elasticsearch, to a specify index `wdg-search-events`.
3. Using elasticsearch queries, we provide data to Kiban dashboard widgets. We will have 3 widgets:
   
   1. **The top 10 most popular brands**: Horizontal bar widget with the number of search related to each brand order by search count; keeping only the 10.
   2. **The number of users by brands names**: Horizontal bar widget with brand names and the number of searches related to each brand
   3. **The Top 3 most popular brands for a given user**: Horizontal bar widget with top 3 brands that have been searched by user. The widget will enable internal users to filter by `user-id`

    > All dashboard will be created using ElasticSearch Aggregations [3].
4. An internal user will navigate to the Kibana dashboard using a custom DNS Name. They will be prompted to authenticate using Cognito.
5. If the user is authenticated, it will have access to the dashboard containing insights about brand related searches.

### 3.4 Additional Notes

The solution satisfy all functional and non-functional requirements. In fact, **Cloudwatch Logs**  and **AWS Opensearch** are managed services which are both secure, highly available and user friendly.


<br>

## 4 References

1. https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CWL_OpenSearch_Stream.html
2. https://docs.aws.amazon.com/opensearch-service/latest/developerguide/cognito-auth.html
3. https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations.html